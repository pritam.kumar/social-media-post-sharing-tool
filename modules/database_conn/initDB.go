package databaseconn

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
)

var DB *gorm.DB

func InitDB() {
	var err error
	// var db *gorm.DB
	Error := godotenv.Load(".env")
	if Error != nil {
		fmt.Println(Error)
	}
	DataSourceName := os.Getenv("DataSourceName")
	DB, err = gorm.Open("mysql", DataSourceName)
	if err != nil {
		fmt.Println(err)
		panic("failed to connect database")
	} else {
		fmt.Println("databse connected Succesfully")
	}
	// Create the database. This is a one-time step.
	// Comment out if running multiple times - You may see an error otherwise
	DB.Exec("CREATE DATABASE IF NOT EXISTS orders_db")
	DB.Exec("USE orders_db")

	// Migration to create tables for Order and Item schema
}
