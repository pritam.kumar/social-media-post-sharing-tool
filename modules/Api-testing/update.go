package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func updateProfile(q http.ResponseWriter, r *http.Request) {
	var idProfile string = mux.Vars(r)["id"]
	id, err := strconv.Atoi(idProfile)

	if err != nil {
		q.WriteHeader(400)
		q.Write([]byte("ID clould not be converted into integer"))
		return
	}
	if id >= len(profiles) {
		q.WriteHeader(400)
		q.Write([]byte("ID Not Found"))

	}
	var updateProfile Profile
	json.NewDecoder(r.Body).Decode(&updateProfile)

	profiles[id] = updateProfile

	q.Header().Set("Content-type", "application/json")
	json.NewEncoder(q).Encode(updateProfile)

}
