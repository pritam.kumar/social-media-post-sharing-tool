package main

import (
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
)

func Dbconnection() {

	envMap, _ := godotenv.Read(".env")
	// create a database object which can be used
	// to connect with database.
	connStr := fmt.Sprintf("%s", envMap["DB_connection"])
	db, err := gorm.Open("mysql", connStr)

	// handle error, if any.
	if err != nil {
		panic(err)
	} else {
		fmt.Print("DATABASE Successfully  connected\n")
	}
	defer db.Close()

	envErr := godotenv.Load(".env")
	if envErr != nil {
		fmt.Printf("Could not load .env file")
		os.Exit(1)
	}

	// database object has  a method Close,
	// which is used to free the resource.
	// Free the resource when the function
	// is returned.
}
