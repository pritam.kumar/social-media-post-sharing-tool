package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func fetchitembyid(q http.ResponseWriter, r *http.Request) {
	var idProfile string = mux.Vars(r)["id"]
	id, err := strconv.Atoi(idProfile)

	if err != nil {
		q.WriteHeader(400)
		q.Write([]byte("ID clould not be converted into integer"))
		return
	}
	if id >= len(profiles) {
		q.WriteHeader(400)
		q.Write([]byte("ID Not Found"))

	}
	Profile := profiles[id]
	q.Header().Set("Content-Type", "application/json")
	json.NewEncoder(q).Encode(Profile)
}
