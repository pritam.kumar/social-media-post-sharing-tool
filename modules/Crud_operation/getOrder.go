package Curd_operation

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	databaseconn "gitlab.com/pritam.kumar/social-media-post-sharing-tool/modules/database_conn"
)

// var db *gorm.DB

func GetOrder(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	inputOrderID := params["orderId"]

	var order Order

	Error := databaseconn.DB.Preload("Items").First(&order, inputOrderID).Error
	if Error != nil {
		fmt.Println(Error)
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", Error))
		return
	} else {
		fmt.Println("order Fetched by ID sucessfully")
	}
	err := json.NewEncoder(w).Encode(order)
	if err != nil {
		fmt.Println(err)
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", err))
		return
	}
}
