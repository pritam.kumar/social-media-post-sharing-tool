package Curd_operation

import (
	"encoding/json"
	"fmt"
	"net/http"

	databaseconn "gitlab.com/pritam.kumar/social-media-post-sharing-tool/modules/database_conn"
)

// var db *gorm.DB

func CreateOrder(w http.ResponseWriter, r *http.Request) {
	var order Order
	Error := json.NewDecoder(r.Body).Decode(&order)
	if Error != nil {
		fmt.Println(Error)
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", Error))
	}
	// Creates new order by inserting records in the `orders` and `items` table
	err := databaseconn.DB.Create(&order).Error
	if err != nil {
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", err))
		fmt.Println(err)
		return
	} else {
		fmt.Println("order created sucessfully")
	}
	w.Header().Set("Content-Type", "application/json")
	err1 := json.NewEncoder(w).Encode(order)
	if err1 != nil {
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", err1))
		fmt.Println(err1)
		return
	}
}
