package Curd_operation

import (
	"encoding/json"
	"fmt"
	"net/http"

	databaseconn "gitlab.com/pritam.kumar/social-media-post-sharing-tool/modules/database_conn"
)

// var db *gorm.DB

func GetOrders(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var orders []Order
	Error := databaseconn.DB.Preload("Items").Find(&orders).Error
	if Error != nil {
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", Error))
		return
	} else {
		fmt.Println("order Fetched sucessfully")
	}
	err := json.NewEncoder(w).Encode(orders)
	if err != nil {
		fmt.Println(err)
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", err))
	}
}
