package Curd_operation

import (
	"encoding/json"
	"fmt"
	"net/http"

	databaseconn "gitlab.com/pritam.kumar/social-media-post-sharing-tool/modules/database_conn"
)

// var db *gorm.DB

func UpdateOrder(w http.ResponseWriter, r *http.Request) {
	// databaseconn.InitDB()

	var updatedOrder Order
	err := json.NewDecoder(r.Body).Decode(&updatedOrder)
	if err != nil {
		fmt.Println(err)
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", err))
	}
	Error := databaseconn.DB.Save(&updatedOrder).Error
	if Error != nil {
		fmt.Println(Error)
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", Error))
		return
	} else {
		fmt.Println("order Update by ID sucessfully")
	}
	w.Header().Set("Content-Type", "application/json")
	err1 := json.NewEncoder(w).Encode(updatedOrder)
	if err1 != nil {
		fmt.Println(err1)
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", err1))
	}
}
