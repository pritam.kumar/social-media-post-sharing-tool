package Curd_operation

import "time"

// var db *gorm.DB

type Instagram struct {
	// gorm.Model
	PostID    uint   `json:"postId" gorm:"column:post_id;primary_key;autoincrement"`
	PostTitle string `json:"postTitle" gorm:"column:post_title"`
	PostDis   string `json:"postDis" gorm:"column:post_dis"`
	// PostAt    time.Time `json:"postAt"`
}

type Order struct {
	// gorm.Model
	OrderID      uint      `json:"orderId" gorm:"primary_key"`
	CustomerName string    `json:"customerName"`
	OrderedAt    time.Time `json:"orderedAt"`
	Items        []Item    `json:"items" gorm:"foreignkey:OrderID"`
}

// Item represents the model for an item in the order
type Item struct {
	// gorm.Model
	LineItemID  uint   `json:"lineItemId" gorm:"primary_key"`
	ItemCode    string `json:"itemCode"`
	Description string `json:"description"`
	Quantity    uint   `json:"quantity"`
	OrderID     uint   `json:"-"`
}
