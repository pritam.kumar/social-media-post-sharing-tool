package Curd_operation

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	databaseconn "gitlab.com/pritam.kumar/social-media-post-sharing-tool/modules/database_conn"
)

// var db *gorm.DB

func DeleteOrder(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	inputOrderID := params["orderId"]
	// Convert `orderId` string param to uint64
	id64, _ := strconv.ParseUint(inputOrderID, 10, 64)
	// Convert uint64 to uint
	idToDelete := uint(id64)

	Error := databaseconn.DB.Where("order_id = ?", idToDelete).Delete(&Item{}).Error
	if Error != nil {
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", Error))
		fmt.Println(Error)
		return
	} else {
		fmt.Println("order deleted in Items table sucessfully")
	}
	Error1 := databaseconn.DB.Where("order_id = ?", idToDelete).Delete(&Order{}).Error
	if Error1 != nil {
		json.NewEncoder(w).Encode(fmt.Sprintf("%s", Error1))
		fmt.Println(Error1)
		return
	} else {
		fmt.Println("order deleted in orders table sucessfully")
	}
	w.WriteHeader(http.StatusDataDelated)

}
