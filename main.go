package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	Curd_operation "gitlab.com/pritam.kumar/social-media-post-sharing-tool/modules/Crud_operation"
	databaseconn "gitlab.com/pritam.kumar/social-media-post-sharing-tool/modules/database_conn"
	// dbop "gitlab.com/pritam/db-api/db_op"
)

// Order represents the model for an order
// Default table name will be `orders`
// var db *gorm.DB

func main() {
	router := mux.NewRouter()
	//create a Instagram post
	// router.HandleFunc("/inspostcreate", dbop.CreateInspost).Methods("POST")
	// Create
	router.HandleFunc("/orders", Curd_operation.CreateOrder).Methods(http.MethodPost)
	// Read
	router.HandleFunc("/orders/{orderId}", Curd_operation.GetOrder).Methods(http.MethodGet)
	// Read-all
	router.HandleFunc("/orders", Curd_operation.GetOrders).Methods(http.MethodGet)
	// Update
	router.HandleFunc("/orders/{orderId}", Curd_operation.UpdateOrder).Methods(http.MethodPut)
	// Delete
	router.HandleFunc("/orders/{orderId}", Curd_operation.DeleteOrder).Methods(http.MethodDelete)
	// Initialize db connection
	databaseconn.InitDB()
	databaseconn.DB.AutoMigrate(&Curd_operation.Order{}, &Curd_operation.Item{}, &Curd_operation.Instagram{})

	// createInspost()
	port := 8080
	fmt.Println("server started on port", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), router))
	// http.ListenAndServe(fmt.Sprintf(":%d", port), router)

}
