# Social Media post sharing tool



## Work Flow

APIs require to 
 - Create new Post for any social media type. 
 - Update/Delete Existing Post.
 every post will have it's time when it will be shared on respected social media account
 (as of now no need to handle the social media account just create a function for each social media, when that function is called we will consider the post is been shared in that account)


## CRON:-
it will run the post on the expected time, 
it should be capable to handle 3 type of concurrency which will be handled by passing the type from command line argument 
1) share post one by one (suppose at X point of time there are Y posts, then those posts will be shared one by one)
2) Share all the post at one (all Y posts should be shared at once) 
3) Share post in chunks, (let's say Y = 15 and one chunk is 5 posts then posts would be shared in 3 chunks each of 5 posts), and one chunk should be called after another
